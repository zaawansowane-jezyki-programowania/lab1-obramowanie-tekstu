t object Main {
  def main(args: Array[String]): Unit = {
    val napisDoObramowania = "Pierwsza linia\nprzerwa i druga linia\njeszcze jedna przerwa i ostatnia linia."
    val wynik = obramuj(napisDoObramowania)
    println(wynik)
  }
  def obramuj(napis: String): String = {
    val linie = napis.split('\n')
    val maxLength = linie.maxBy(_.length).length
    val ramka = "*" * (maxLength + 4)
    val obramowanyNapis = linie.map(line => s"* $line${" " * (maxLength - line.length)} *").mkString("\n")

    s"$ramka\n$obramowanyNapis\n$ramka"
  }
}